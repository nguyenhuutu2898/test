import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { NeoService } from '../services/neo.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
})
export class SearchFormComponent implements OnInit {
  rfContact!: FormGroup;
  maxDate!: Date;
  spinner: boolean = false;
  @Output() emitSearchResults = new EventEmitter<any>();

  constructor(private fb: FormBuilder, private neoService: NeoService) {}

  ngOnInit(): void {
    this.rfContact = this.fb.group({
      start: '',
      end: '',
    });

    this.rfContact.get('start')?.valueChanges.subscribe((x: Date) => {
      this.maxDate = new Date(x.getFullYear(), x.getMonth(), x.getDate() + 7);
    });

    this.rfContact.get('end')?.valueChanges.subscribe((x: Date) => {
      if (x) {
        if (x > this.maxDate) {
          alert('End date up to 7 days from start date');
          this.rfContact.get('end')?.setValue(this.maxDate);
        } else {
          this.spinner = true;
          this.neoService
            .search(
              this.formatDate(this.rfContact.value.start),
              this.formatDate(x)
            )
            .subscribe((res) => {
              if (res) {
                this.spinner = false;
                this.emitSearchResults.emit(res);
              }
            });
        }
      }
    });
  }

  formatDate(date: Date) {
    if (!date) {
      return;
    }
    console.log('date', date);
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    return `${year}-${month}-${day}`;
  }

  onSubmit() {
    console.log(this.rfContact);
  }
}
