import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'test';
  data: any = [];

  searchResults(event: any) {
    const temp = [];
    for (const key in event.near_earth_objects) {
      temp.push({ key, value: event.near_earth_objects[key] });
    }

    this.data = temp.sort((a: any, b: any) => {
      if (a.key < b.key) {
        return -1;
      }
      if (a.key > b.key) {
        return 1;
      }
      return 0;
    });
  }
}
