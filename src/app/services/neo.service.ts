import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class NeoService {
  constructor(private http: HttpClient) {}

  search(start_date: any, end_date: any) {
    return this.http.get<any[]>(
      `https://api.nasa.gov/neo/rest/v1/feed?start_date=${start_date}&end_date=${end_date}&api_key=1Gv7JX1nN6vv4x1QkmNcwGVb2fvyb1FC9clgKoKu`
    );
  }
}
